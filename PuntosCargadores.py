import json
from FuncionesCargadores import *
with open ("PuntosCargadores.json") as fichero:
    datos=json.load(fichero)
#Menu que incluye los diferentes ejercicios
while True:
    print('''
        1. MUESTRA LAS LOCALIDADES QUE TIENEN CARGADORES
        2. MUESTRA LAS LOCALIDADES Y LA CANTIDAD DE PUNTOS DE RECARGA
        3. INTRODUCE UNA CADENA Y TE MUESTRA LAS LOCALIDADES QUE CONTIENEN ESA CADENA
        4. INTRODUCE UNA LOCALIDAD Y MUESTRA UNA DIRECCION, INFORMACIÓN, PUNTO DE REFERENCIA y LOCALIZACION 
        5. MOSTRAR APARCAMIENTOS CLASIFICADOS EN PÚBLICO, PRIVADO O EXCLUSIVO
        0. SALIR
        ''')
    opcion=int(input("Introduce una Opcion: "))
#Muestra todas las localidades que tienen Punto de Recarga para Coches Electricos
    if opcion==1:
       for localidad in mostrar_localidades(datos):
           print(localidad)
#Nos muestras cada localidad con los Puntos de Recarga para Coches Eléctricos
    if opcion==2:
        for cantidad,localidad in zip (cargadores_localidad(datos)[1::2],cargadores_localidad(datos)[::2]):
            if cantidad==1:
                print("%s tiene %s Punto de Carga"%(localidad,cantidad))
            else:
                print("%s tiene %s Puntos de Carga"%(localidad,cantidad))
#Introduciendo el nombre completo o una parte del mismo nos muestra las localidades con ese nombre o que contienen dicha cadena
    if opcion==3:       
        nombre_localidad=input("Introduce el Nombre o parte del Nombre de una Localidad: ")
        for nombre in busca_localidad(datos,nombre_localidad):
            print(nombre)#Introduciendo el nombre de la localidad nos muestra informacion relacionada con dicho punto
    if opcion==4:
        nombre_localidad=input("Nombre de la localidad: ")
        print("%s tiene estos puntos: "%(nombre_localidad.capitalize()))
        for dato in puntos_localidad(datos,nombre_localidad)[1]:
            print('''
            Direccion: %s
            Info.:%s
            Referencia: %s
            Localizacion: https://www.openstreetmap.org/#map=19/%s/%s'''%(dato[0],dato[1],dato[2],dato[3],dato[4]))
        for dato in puntos_localidad(datos,nombre_localidad)[0]:
            print('''
            Direccion: %s
            Info.: %s 
            Localizacion: https://www.openstreetmap.org/#map=19/%s/%s''' %(dato[0],dato[1],dato[2],dato[3]))
#Nos muestra los diferentes Puntos de Recarga para Coches Electricos segun sean publicos,privados o exclusivos
    if opcion==5:
        print("*********************CARGADORES PUBLICOS******************")
        for nombre in tipo_cargador(datos)[0]:
            print("                     -------%s-------                  "%nombre)
            for calle in tipo_cargador(datos)[1]:
                if nombre in calle[0]:
                    print(calle[1])
        print()
        print("*********************CARGADORES PRIVADOS******************")
        for nombre in tipo_cargador(datos)[2]:
            print("                     -------%s-------                  "%nombre)
            for calle in tipo_cargador(datos)[3]:
                if nombre in calle[0]:
                    print(calle[1])
        print("*********************CARGADORES EXCLUSIVOS****************")
        for nombre in tipo_cargador(datos)[4]:
            print("                     -------%s-------                  "%nombre)
            for calle in tipo_cargador(datos)[5]:
                if nombre in calle[0]:
                    print(calle[1])
    if opcion==0:
        break
