#Listar el nombre de todas las localidades
def mostrar_localidades(datos):
    localidades=[]
    for cont in range(len(datos)):
        if datos[cont]['fields']['localidad'] not in localidades:
            localidades.append(datos[cont]['fields']['localidad'])    
    return localidades
#Muestra las localidades y la cantidad de cargadores que tiene
def cargadores_localidad(datos):
    todas_localidades=[]
    localidades=[]
    contar_localidad=[]
    #Guarda en una lista todas las localidades
    for cont in range(len(datos)):
        todas_localidades.append(datos[cont]['fields']['localidad'])
        if datos[cont]['fields']['localidad'] not in localidades:
            localidades.append(datos[cont]['fields']['localidad'])
    for localidad in localidades:
        contar_localidad.extend([localidad,todas_localidades.count(localidad)])
    return contar_localidad
    #Introduce una cadena y muestra las localidades que empiezan por esa subcadena
def busca_localidad(datos,nombre_localidad):
    localidad=[]
    for cont in range(len(datos)):
        if datos[cont]['fields']['localidad'].startswith(nombre_localidad.capitalize()) and datos[cont]['fields']['localidad'] not in localidad:
            localidad.append(datos[cont]['fields']['localidad'])
    return localidad
#Introduce el nombre de una localidad e indica en que puntos se encuentra el cargador
def puntos_localidad(datos,nombre_localidad):
    localidades=[]
    localidades2=[]
    for cont in range(len(datos)):
        if datos[cont]['fields']['localidad']==nombre_localidad and 'datospersonales' in datos[cont]['fields']:
            puntos=[]
            puntos.extend([datos[cont]['fields']['calle'],datos[cont]['fields']['nombre_del_organismo'],datos[cont]['fields']['datospersonales'],datos[cont]['fields']['latitud'],datos[cont]['fields']['longitud']])
            localidades.append(puntos)
        if datos[cont]['fields']['localidad']==nombre_localidad and 'datospersonales' not in datos[cont]['fields']:
            puntos=[]
            puntos.extend([datos[cont]['fields']['calle'],datos[cont]['fields']['nombre_del_organismo'],datos[cont]['fields']['latitud'],datos[cont]['fields']['longitud']])
            localidades2.append(puntos)
    return localidades2,localidades
    #Muestra los diferentes Puntos de Recarga para Coches Electricos segun sea publico,privado o exclusivo
def tipo_cargador(datos):
    publico=[]
    privado=[]
    exclusivo=[]
    loc_publico=[]
    loc_privado=[]
    loc_exclusivo=[]
    for cont in range(len(datos)):
        # Puntos de Recarga Publicos
        if 'público' in datos[cont]['fields']['nombre_del_organismo']:
            parking=[]
            parking.extend([datos[cont]['fields']['localidad'],datos[cont]['fields']['calle']])
            publico.append(parking)
            if datos[cont]['fields']['localidad'] not in loc_publico:
                loc_publico.append(datos[cont]['fields']['localidad'])
        #Puntos de Recarga Privados
        if 'privado' in datos[cont]['fields']['nombre_del_organismo']:
            parking=[]
            parking.extend([datos[cont]['fields']['localidad'],datos[cont]['fields']['calle']])
            privado.append(parking)
            if datos[cont]['fields']['localidad'] not in loc_privado:
                loc_privado.append(datos[cont]['fields']['localidad'])
        #Puntos de Recarga Exclusivos
        if 'exclusivo' in datos[cont]['fields']['nombre_del_organismo']:
            parking=[]
            parking.extend([datos[cont]['fields']['localidad'],datos[cont]['fields']['calle']])
            exclusivo.append(parking)
            if datos[cont]['fields']['localidad'] not in loc_exclusivo:
                loc_exclusivo.append(datos[cont]['fields']['localidad'])
    return loc_publico,publico,loc_privado,privado,loc_exclusivo,exclusivo