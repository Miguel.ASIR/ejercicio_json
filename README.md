# ejercicio_json

El primer ejercicio **listar informacion** voy a listar las localidades que tienen Puntos de Recarga de Coches Electricos, en el ejercicio **Contar información** voy a mostrar la cantidad de puntos de recarga que tiene cada localidad, en el ejercicio **Buscar o filtrar información** vamos a introducir una cadena o parte de ella y saldrá el nombre completo de la localidad o el nombre de la localidad que empiece por la subcadena introducida,en el ejercicio **Buscar información relacionada** introduciremos el nombre de la localidad y nos indicara la dirección del Punto de Recarga y si tiene algún punto de referencia para ayudarnos a encontrar el Punto y su localización en OpenStreetMap y en **El ejercicio libre** Mostraremos los diferentes Puntos de recargar según sean Públicos,Privados o Exclusivos.


